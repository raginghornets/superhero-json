const requestUrl = 'https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json'; 
const request = new XMLHttpRequest();
request.open('GET', requestUrl);
request.responseType = 'json';
request.send();
request.onload = function () {
  const superHeroes = request.response;
  document.body.appendChild(createHeroHeader(superHeroes));
  document.body.appendChild(createHeroSection(superHeroes));
}

function createTextTag(tag, textContent) {
  const newTag = document.createElement(tag);
  newTag.textContent = textContent;
  return newTag;
}

function createListTag(tag, list) {
  const newList = document.createElement(tag);
  for (let i = 0; i < list.length; i++) {
    newList.appendChild(createTextTag('li', list[i]));
  }
  return newList;
}

function createHeroHeader(jsonObj) {
  const header = document.createElement('header');
  header.appendChild(createTextTag('h1', jsonObj['squadName']));
  header.appendChild(createTextTag('p', 'Hometown: ' + jsonObj['homeTown'] + ' // Formed: ' + jsonObj['formed']));
  return header;
}

function createHeroSection(jsonObj) {
  const section = document.createElement('section');
  const heroes = jsonObj['members'];
  for (let i = 0; i < heroes.length; i++) {
    section.appendChild(createHeroCard(heroes[i]));
  }
  return section;
}

function createHeroCard(hero) {
  const heroCard = document.createElement('article');
  heroCard.appendChild(createTextTag('h2', hero.name));
  heroCard.appendChild(createTextTag('p', 'Secret identity: ' + hero.secretIdentity));
  heroCard.appendChild(createTextTag('p', 'Age: ' + hero.age));
  heroCard.appendChild(createTextTag('p', 'Superpowers:'));
  heroCard.appendChild(createListTag('ul', hero.powers));
  return heroCard;
}